## Required
Python3  
click


## USAGE

`python ascii_linter.py --help` to get a list of available commands  
`python ascii_linter.py <command> --help` on any command to see usage
