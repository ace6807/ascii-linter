from collections import namedtuple
import click

NonASCII = namedtuple('NonASCII', ['line_no', 'col_no', 'char'])


@click.group()
def cli():
    pass


def print_non_ascii_chars(non_ascii_chars):
    print(f"Total Non-ASCII chars found: {len(non_ascii_chars)}")
    print(f"------------------------------")
    for non_ascii in non_ascii_chars:
        print(f"Non-Ascii character {non_ascii.char} at line {non_ascii.line_no}, column {non_ascii.col_no}")
    print()


def interactively_replace_bad_chars(file_name):
    file_chars = open(file_name).read()
    clean_chars = []
    for char in file_chars:
        if char != " " and (ord(char) < 0 or ord(char) > 127):
            print()
            cleaned_char = input(f"Replace {char} with: ")
            clean_chars.append(cleaned_char)
        else:
            print(char, end='')
            clean_chars.append(char)

    return clean_chars


@cli.command()
@click.argument('file_name')
def find_non_ascii(file_name):
    non_ascii_chars = []
    with open(file_name) as fin:
        cur_line = 1
        for line in fin:
            cur_char = 1
            for char in line:
                if char != " " and (ord(char) < 0 or ord(char) > 127):
                    non_ascii_chars.append(NonASCII(cur_line, cur_char, char))
                cur_char = cur_char + 1
            cur_line = cur_line + 1

    print_non_ascii_chars(non_ascii_chars)
    return non_ascii_chars


@cli.command()
@click.argument('file_name')
@click.argument('new_file_name')
def fix_non_ascii(file_name, new_file_name):
    try:
        with open(new_file_name, 'w') as clean_file:
            cleaned_chars = interactively_replace_bad_chars(file_name)
            clean_file.write("".join(cleaned_chars))

        print(f"Fixed file saved to: {new_file_name}")

    except FileNotFoundError:
        print(f"{file_name} not found. Make sure the path specified exists and is readable.")

    except PermissionError:
        print(f"{new_file_name} already exists and is not able to be modified.")

    except:
        print(f"Process failed.")


if __name__ == '__main__':
    cli()
